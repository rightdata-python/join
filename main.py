from typing import List, Optional
from fastapi import FastAPI
from pydantic import BaseModel
#importing Models as definitions from the files
from Merge_version1 import mergeDatasets
from Merge_Version2 import mergeDatasets2

#assigning FastAPI as app
app = FastAPI()

#Defining Base Model
#defining the inputs
class MergeRequest(BaseModel):
    #driver dataset location
    driverDatasetLocation: str
    #Reference dataset location
    referenceDatasetLocation: str
    #ouput file storage location
    mergedDatasetLocation: str
    mergeType: str
    #columnNames: List[dict]g
    columnName1 : str
    columnName2: str
    columnName3: str

#defining the endpoint for fuzzy matcher Model
@app.post("/MergeRequestAPI2")
#calling the functions with given inputs
async def MergeRequestAPI(mr:MergeRequest):
    #Result = fuzzyMatchDef(fm.driverDatasetLocation,fm.referenceDatasetLocation,fm.fuzzyMatchedLocation,fm.threshold,fm.driverColumnNames,fm.referenceColumnNames)
    Result = mergeDatasets(mr.driverDatasetLocation, mr.referenceDatasetLocation, mr.mergedDatasetLocation, mr.mergeType, mr.columnName1, mr.columnName2)
    # ...
    # Runs the fuzzy matcher algorithm
    # ...
    return {f"{Result}"}