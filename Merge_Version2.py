import pandas as pd


def mergeDatasets2(driverDatasetLocation, referenceDatasetLocation, mergedDatasetLocation, mergeType, columnNames):
    print("Merging Dataset Started")
    # Read Driver dataset or left dataset as dataframe, as the file is in csv take delimiter as input and index_col 0 so that no index is generated when csv is read as dataframe
    DriverDataset = pd.read_csv(driverDatasetLocation) #, header='infer',   index_col=[0])
    print(DriverDataset.shape)
    # Read Reference dataset or right dataset as dataframe
    ReferenceData = pd.read_csv(referenceDatasetLocation)  # , header='infer', index_col=[0])
    # Extracting the columnnames of driver dataset which came as inputs to drivercolumnnames list
    drivercolumnnames = []
    #for i, x in enumerate(columnNames):
    for x in columnNames:
        # DriverDataset[x['driverColumn']] = DriverDataset[x['driverColumn']].astype(str)
        drivercolumnnames.append(x['driverColumn'])

    # Extracting the columnnames of reference dataset which came as inputs to referencecolumnnames list
    referencecolumnnames = []
    for y in columnNames:
        # ReferenceData[x['referenceColumn']] = ReferenceData[x['referenceColumn']].astype(str)
        referencecolumnnames.append(y['referenceColumn'])
    if drivercolumnnames == referencecolumnnames:
        mergedDataset = pd.merge(DriverDataset, ReferenceData, on=drivercolumnnames, how=mergeType)
        print(mergeType + " joining of 2 datasets is successful")
    else:
        mergedDataset = pd.merge(DriverDataset, ReferenceData, left_on=drivercolumnnames, right_on=referencecolumnnames, how=mergeType).drop(referencecolumnnames, axis=1)
        print(mergeType + "joining of 2 datasets is successful")

    mergedDataset.to_csv(mergedDatasetLocation)
    # after successfully executing the algorithm print success statement
    return "Successfully completed " + mergeType + " join on given datasets." + " Shape of the " + mergeType + " join Dataset: " + str(mergedDataset.shape)
#mergeDatasets(data1.txt, data2.txt, leftjoin.csv, mergeType, columnName1, columnName2)