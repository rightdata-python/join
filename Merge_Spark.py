from pyspark.sql import SparkSession
print("Application Started")
# get a spark session.
spark = SparkSession.builder.appName("MergeSparkTest").getOrCreate()
print("spark session Started")

#DriverDataset = spark.read.option("header", True).option("inferSchema" , True).csv("C:\\Users\\kisho\\Desktop\\BinduAllWork\\RIGHTDATA\\DataJoinProject\\SampleJoindata_Big_1.csv")
DriverDataset = spark.read.option("header", True).option("inferSchema" , True).csv("C:\\Users\\kisho\\PycharmProjects\\DataJoinProject\\data1.txt")
print(DriverDataset.count(),DriverDataset.columns)

#ReferenceData = spark.read.option("header", True).option("inferSchema" , True).csv("C:\\Users\\kisho\\Desktop\\BinduAllWork\\RIGHTDATA\\DataJoinProject\\SampleJoindata_Big_2.csv")
ReferenceData = spark.read.option("header", True).option("inferSchema" , True).csv("C:\\Users\\kisho\\PycharmProjects\\DataJoinProject\\data2.txt")
print(ReferenceData.count(),ReferenceData.columns)

# columnsNamesDD = [DriverDataset.columns]
#
# DriverDatasetDF = spark.createDataFrame(data=DriverDataset, schema = columnsNamesDD)
# #DriverDatasetDF.printSchema()
# #DriverDatasetDF.show(truncate=False)
#
# columnsNamesRD = [DriverDataset.columns]
#
# ReferenceDataDF = spark.createDataFrame(data=ReferenceData, schema = columnsNamesRD)
# #DriverDatasetDF.printSchema()
# #DriverDatasetDF.show(truncate=False)

#MergeDF = DriverDatasetDF.join(ReferenceDataDF,DriverDatasetDF.use_id ==  ReferenceDataDF.use_id,"outer").show(truncate=False)
print("About to Join")
#MergeDF = DriverDataset.join(ReferenceData,DriverDataset['use_id','user_id'] ==  ReferenceData['use_id','user_id'],"outer")
#
MergeDF = DriverDataset.join(ReferenceData, ['use_id', 'use_id'], "outer")
print(MergeDF.count(),len(MergeDF.columns))#.show(truncate=False)
print("Join complete")
MergeDF.show(truncate=False)
#df.write.csv("/tmp/spark_output/datacsv")
# newNames = ['ID_x','ID_y','c0','Date_x','Time_x','Global_active_power_x','Global_reactive_power_x','Voltage_x','Global_intensity_x','Sub_metering_1_x','Sub_metering_2_x','Sub_metering_3_x','_x_x ','Date_x_x','Time_x_x','Voltage_x_x','Global_intensity_x_x']
# df= MergeDF.toDF(*newNames)
#df.show()
df = MergeDF.toPandas()
print(df.shape)
print(df.head())
df.to_csv("outputjoin.csv")
# // You can also use below

#df.write.format("csv").save("C:\\Users\\kisho\\Desktop\\BinduAllWork\\RIGHTDATA\\DataJoinProject\\output2.csv")
#MergeDF.write.csv("output2.csv") #C:\\Users\\kisho\\Desktop\\BinduAllWork\\RIGHTDATA\\DataJoinProject\\