import pandas as pd


def mergeDatasets(driverDatasetLocation, referenceDatasetLocation, mergedDatasetLocation, mergeType, columnName1, columnName2):
    print("Merging Dataset Started")
    # Read Driver dataset or left dataset as dataframe, as the file is in csv take delimiter as input and index_col 0 so that no index is generated when csv is read as dataframe
    DriverDataset = pd.read_csv(driverDatasetLocation)#, header='infer',   index_col=[0])
    print(DriverDataset.shape)
    # Read Reference dataset or right dataset as dataframe
    ReferenceData = pd.read_csv(referenceDatasetLocation)#, header='infer', index_col=[0])
    # if mergeType = "inner":
    #     mergedDataset = pd.merge(DriverDataset, ReferenceData, on=columnName, how='inner')
    #     print(" Inner joining of 2 datasets is successful")
    # elif mergeType = "outer":
    #     mergedDataset = pd.merge(DriverDataset, ReferenceData, on=columnName, how='outer')
    #     print(" Outer joining of 2 datasets is successful")
    #
    # elif mergeType = "left":
    #     mergedDataset = pd.merge(DriverDataset, ReferenceData, on=columnName, how='left')
    #     print(" Left joining of 2 datasets is successful")
    #
    # elif mergeType = "right":
    #     mergedDataset = pd.merge(DriverDataset, ReferenceData, on=columnName, how='right')
    #     print(" Right joining of 2 datasets is successful")

    if columnName1 == columnName2:
        mergedDataset = pd.merge(DriverDataset, ReferenceData, on=columnName1, how = mergeType)
        print( mergeType + " joining of 2 datasets is successful")
    else:
        mergedDataset = pd.merge(DriverDataset, ReferenceData, left_on=columnName1, right_on=columnName2, how=mergeType).drop(columnName2, axis=1)
        print(mergeType + "joining of 2 datasets is successful")

    mergedDataset.to_csv(mergedDatasetLocation)
    # after successfully executing the algorithm print success statement
    return "Successfully completed "+mergeType+ " join on given datasets." + " Shape of the " + mergeType + " join Dataset: " + str(mergedDataset.shape)
#mergeDatasets(data1.txt, data2.txt, leftjoin.csv, mergeType, columnName1, columnName2)